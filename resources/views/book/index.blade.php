<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Biblioteca TecMM</title>
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}" type="image/x-icon">
    <link href="{{ asset('/css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/bootstrap.min.css') }}" rel="stylesheet">
    <script src="{{ asset('js/popper.min.js') }}"></script>
    <script src="{{ asset('js/jquery-3.2.1.slim.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
</head>
<body>
    <nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
        <a class="navbar-brand" href="/">
            <img src="{{ asset('favicon.ico') }}" width="30" height="30" class="d-inline-block align-top" alt="">
            Biblioteca TecMM 
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" 
            data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" 
            aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarsExampleDefault">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="/">Lista</a>
                </li>
            </ul>
        </div>
    </nav>

    <div class="modal fade" id="agregarModal" tabindex="-1" role="dialog" aria-labelledby="agregarModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="agregarModalLabel">Formularío</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"><i class="fa fa-times" aria-hidden="true"></i></span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="/" method="get">
                        <input type="text" class="form-control" name="book_title" 
                        placeholder="Título" maxlength="45" required autocomplete="off">
                        <br>
                        <input type="text" class="form-control" name="book_author" 
                        placeholder="Autor" maxlength="45" required autocomplete="off">
                        <br>
                        <input type="date" class="form-control" name="book_date" 
                        required>
                        <br>
                        <p class="text-right">
                            <button type="button" class="btn btn-outline-dark" data-dismiss="modal">Cerrar</button>
                            <button type="submit" class="btn btn-outline-primary">Agregar libro</button>
                        </p>
                    </form>
                </div>
            </div>
        </div>
    </div>
 
    <br><br><br>

    <div class="container">
        <h1 class="display-4">Libros 
        <button type="button" class="btn btn-outline-success" data-toggle="modal" 
        data-target="#agregarModal"><i class="fa fa-plus" aria-hidden="true"></i> Agregar libro</button>
        </h1>
        <table class="table table-hover">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Título</th>
                    <th scope="col">Autor</th>
                    <th scope="col">Fecha de publicación</th>
                    <th scope="col"></th>
                </tr>
            </thead>
            <tbody>
                @php $a = 1 @endphp
                @foreach($books as $book)
                <tr>
                    <th> @php echo $a++ @endphp </th>
                    <td> {{$book->name}} </td>
                    <td> {{$book->author}} </td>
                    <td> {{$book->date}} </td>
                    <td>
                        <div class="row">
                            <div class="col-md-auto">
                                <button type="submit" class="btn btn-outline-info btn-sm" 
                                data-toggle="modal" data-target="#{{$book->id}}Modal">
                                <i class="fa fa-pencil" aria-hidden="true"></i> Modificar</button>
                            </div>
                            <div class="col-md-auto">
                                <form action="/delete/{{$book->id}}" method="get">
                                    <button type="submit" class="btn btn-outline-danger btn-sm">
                                    <i class="fa fa-times" aria-hidden="true"></i> Eliminar</button>
                                </form>
                            </div>
                        </div>
                    </td>
                </tr>
                <div class="modal fade" id="{{$book->id}}Modal" tabindex="-1" role="dialog"
                aria-labelledby="{{$book->id}}ModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="{{$book->id}}ModalLabel">Modificar información</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true"><i class="fa fa-times" aria-hidden="true"></i></span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <form action="/update/{{$book->id}}" method="get">
                                    <input type="text" class="form-control" name="book_title" 
                                    placeholder="Título" maxlength="45" required autocomplete="off" value="{{$book->name}}">
                                    <br>
                                    <input type="text" class="form-control" name="book_author" 
                                    placeholder="Autor" maxlength="45" required autocomplete="off" value="{{$book->author}}">
                                    <br>
                                    <input type="date" class="form-control" name="book_date" 
                                    required value="{{$book->date}}">
                                    <br>
                                    <p class="text-right">
                                        <button type="button" class="btn btn-outline-dark" data-dismiss="modal">Cerrar</button>
                                        <button type="submit" class="btn btn-outline-info">Modificar libro</button>
                                    </p>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </tbody>
        </table>
    </div>

    <br><br><br>

</body>
</html>