<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Biblioteca TecMM</title>
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}" type="image/x-icon">
    <link href="{{ asset('/css/bootstrap.min.css') }}" rel="stylesheet">
    <script src="{{ asset('js/popper.min.js') }}"></script>
    <script src="{{ asset('js/jquery-3.2.1.slim.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
</head>
<body>
    <nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
        <a class="navbar-brand" href="#">
            <img src="{{ asset('favicon.ico') }}" width="30" height="30" class="d-inline-block align-top" alt="">
            Biblioteca TecMM 
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" 
            data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" 
            aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarsExampleDefault">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="#">Lista</a>
                </li>
                <li class="nav-item">
                    <button type="button" class="btn btn-link nav-link" data-toggle="modal" 
                        data-target="#agregarModal">
                        Agregar libro <i class="fa fa-plus" aria-hidden="true"></i>
                    </button>
                </li>
            </ul>
        </div>
    </nav>

    <br><br><br>

    <div class="modal fade" id="agregarModal" tabindex="-1" role="dialog" aria-labelledby="agregarModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="agregarModalLabel">Formularío</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="form-group">
                            <input type="text" class="form-control" id="book_title" placeholder="Título" maxlength="45" require>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" id="book_author" placeholder="Autor" maxlength="45" require>
                        </div>
                        <div class="form-group">
                            <input type="date" class="form-control" id="book_date" require>
                        </div>
                        <p class="text-right">
                            <button type="submit" class="btn btn-outline-primary pull-right">Agregar libro</button>
                        </p>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <h1 class="display-4">Libros</h1>
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Título</th>
                        <th scope="col">Autor</th>
                        <th scope="col">Año de publicación</th>
                        <th scope="col"></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th scope="row">1</th>
                        <td>Mark</td>
                        <td>Otto</td>
                        <td>@mdo</td>
                        <td>
                            <button type="button" class="btn btn-outline-danger btn-sm">Eliminar</button>
                        </td>
                    </tr>
                </tbody>
            </table>
    </div>

    <br><br><br>

</body>
</html>