<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Book;
use Redirect;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request){

        if (request('book_title')!="" && 
            request('book_author')!="" &&
            request('book_date')!=""){
                $b = new Book;
                $b->name = request('book_title');
                $b->author = request('book_author');
                $b->date = request('book_date');
                $b->save();
                return \Redirect::back();
        }
        $books = DB::table('books')->get();
        return view('book.index', ['books' => $books]);

    }

    public function delete($id)
    {   
        echo "Cargando...";
        $b = Book::find($id);
        $b->delete();
        return \Redirect::back();
    }

    public function update(Request $request, $id)
    {
        echo "Cargando...";
        if (request('book_title')!="" && 
        request('book_author')!="" &&
        request('book_date')!=""){
            $b = Book::find($id);
            $b->name = request('book_title');
            $b->author = request('book_author');
            $b->date = request('book_date');
            $b->save();
            return \Redirect::back();
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        /*$b = new Book;
        $b->name = Input::get(request('book_title'));
        $b->author = Input::get(request('book_author'));
        $b->date = Input::get(request('book_date'));
        $b->save();
        return Redirect::back();*/
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        /*$b = Book::find($id);
        $b->delete();*/
    }
}
