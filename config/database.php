<?php

return [

    'default' => 'sqlite',

    'connections' => [
        'sqlite' => [
            'driver' => 'sqlite',
            'database' =>  database_path('database.sqlite'),
            'prefix' => '',
        ]
    ],
];
